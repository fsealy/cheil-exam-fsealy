# Cheil Backend

Este desarrollo fue realizado en la herramienta Visual Studio 2022
y se utilizaron los Frameworks:
- Entity Framework 6.0
- MySql.EntityFrameworkCore 6.0
- Net Core 6.0

## Configuración de Base de Datos
Se debe configurar la base de datos en el archivo (appsettings.json)

## Ejecución
Se debe clonar este repositorio desde Visual Studio 2022, abrir la solución y ejecutarla.

## Demostración
http://sealy-test.ddns.net:88/swagger/index.html
