﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CheilSolution.Data
{
    public partial class CheilContext : DbContext
    {
        public CheilContext()
        {
        }

        public CheilContext(DbContextOptions<CheilContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Hotel> Hotels { get; set; }
        public virtual DbSet<Score> Scores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                string connString = configuration.GetConnectionString("CheilConn");
                optionsBuilder.UseMySQL(connString);

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Hotel>(entity =>
            {
                entity.ToTable("hotel");

                entity.Property(e => e.HotelName).HasMaxLength(100);

                entity.Property(e => e.Picture1).HasMaxLength(250);

                entity.Property(e => e.Picture2).HasMaxLength(250);

                entity.Property(e => e.Picture3).HasMaxLength(250);

                entity.Property(e => e.HotelDesc).HasMaxLength(500);

                entity.Property(e => e.Price).HasColumnType("decimal(10,2)");

            });

            modelBuilder.Entity<Score>(entity =>
            {
                entity.ToTable("score");

                entity.HasIndex(e => e.HotelId, "Fk_Score_Hotel_idx");

                entity.Property(e => e.ScoreComment).HasMaxLength(100);

                entity.HasOne(d => d.Hotel)
                    .WithMany(p => p.Scores)
                    .HasForeignKey(d => d.HotelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_Score_Hotel");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
