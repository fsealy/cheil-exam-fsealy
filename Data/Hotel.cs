﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CheilSolution.Data
{
    public partial class Hotel
    {
        public Hotel()
        {
            Scores = new HashSet<Score>();
        }

        public int HotelId { get; set; }
        public string HotelName { get; set; }

        public string HotelDesc { get; set; }
        public int Category { get; set; }
        public decimal? Price { get; set; }
        public string Picture1 { get; set; }
        public string Picture2 { get; set; }
        public string Picture3 { get; set; }

        public int ScorePoint { get; set; }

        public virtual ICollection<Score> Scores { get; set; }
    }
}
