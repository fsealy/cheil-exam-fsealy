﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CheilSolution.Data
{
    public partial class Score
    {
        public int ScoreId { get; set; }
        public int ScorePoint { get; set; }
        public string ScoreComment { get; set; }
        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }
    }
}
