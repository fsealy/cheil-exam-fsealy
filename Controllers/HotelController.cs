﻿using CheilSolution.Data;
using Microsoft.AspNetCore.Mvc;

namespace CheilSolution.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        [HttpGet("getAll")]
        [Produces("application/json")]
        public IEnumerable<Hotel> getData() {
            var context = new CheilContext();
            var hotels = context.Hotels.ToList();
            return hotels;
        }

        [HttpGet("orderByPrice")]
        [Produces("application/json")]
        public IEnumerable<Hotel> Price([FromQuery] string order = "A")
        {
            var context = new CheilContext();
            var hotels = from h in context.Hotels
                         select h;
            if (order.ToUpper().Equals("A"))
            {
                return hotels.OrderBy(x => x.Price).ToList();
            } else
            {
                return hotels.OrderByDescending(x => x.Price).ToList();
            }
        }

        [HttpGet("getByFilters")]
        [Produces("application/json")]
        public IEnumerable<Hotel> Filters([FromQuery] int category = 0, int qualify = 0)
        {
            var context = new CheilContext();
            var query = from h in context.Hotels
                         select h;
            if (category > 0)
            {
                query = query.Where(x => x.Category == category);
            }
            if (qualify > 0)
            {
                var scores = from s in context.Scores
                             where s.ScorePoint == qualify
                             select s.HotelId;
                query = query.Where(x => scores.ToList().Contains(x.HotelId));
            }
            return query.ToList();
            
        }

        [HttpPost("add")]
        [Produces("application/json")]
        public void add([FromBody] Hotel body)
        {
            var context = new CheilContext();
            context.Hotels.Add(body);
            context.SaveChanges();
        }

        [HttpPost("update")]
        [Produces("application/json")]
        public void update([FromBody] Hotel body)
        {
            var context = new CheilContext();
            try
            {
                var hotel = context.Hotels.First(x => x.HotelId == body.HotelId);
                if (hotel != null)
                {
                    if (hotel != null)
                    {
                        hotel.HotelName = body.HotelName;
                        hotel.Picture1 = body.Picture1;
                        hotel.Picture2 = body.Picture2;
                        hotel.Picture3 = body.Picture3;
                        hotel.Price = body.Price;
                        hotel.ScorePoint = body.ScorePoint;
                        context.Hotels.Update(hotel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        [HttpDelete("delete/{id}")]
        [Produces("application/json")]
        public void delete(int id)
        {
            var context = new CheilContext();
            try
            {
                var hotel = context.Hotels.First(x => x.HotelId == id);
                if (hotel != null)
                {
                    context.Hotels.Remove(hotel);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
        }

        [HttpGet("find/{id}")]
        [Produces("application/json")]
        public Hotel findById(int id)
        {
            var context = new CheilContext();
            try {
                var hotel = context.Hotels.First(x => x.HotelId == id);
                if (hotel != null)
                {
                    return hotel;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
